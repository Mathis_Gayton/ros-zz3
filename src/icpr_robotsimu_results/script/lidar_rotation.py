#!/usr/bin/env python
import rospy
from std_msgs.msg import Float64

rospy.init_node('lidar_rotation')
pub = rospy.Publisher('/laser_velocity_controller/command', Float64, queue_size=10)
rate = rospy.Rate(1)

while not rospy.is_shutdown():
    rotation = 20
    pub.publish(rotation)
    rate.sleep()
