# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/eolion/ros-zz3/src/husky/husky_base/src/horizon_legacy/Logger.cpp" "/home/eolion/ros-zz3/build/husky/husky_base/CMakeFiles/horizon_legacy.dir/src/horizon_legacy/Logger.cpp.o"
  "/home/eolion/ros-zz3/src/husky/husky_base/src/horizon_legacy/Message.cpp" "/home/eolion/ros-zz3/build/husky/husky_base/CMakeFiles/horizon_legacy.dir/src/horizon_legacy/Message.cpp.o"
  "/home/eolion/ros-zz3/src/husky/husky_base/src/horizon_legacy/Message_cmd.cpp" "/home/eolion/ros-zz3/build/husky/husky_base/CMakeFiles/horizon_legacy.dir/src/horizon_legacy/Message_cmd.cpp.o"
  "/home/eolion/ros-zz3/src/husky/husky_base/src/horizon_legacy/Message_data.cpp" "/home/eolion/ros-zz3/build/husky/husky_base/CMakeFiles/horizon_legacy.dir/src/horizon_legacy/Message_data.cpp.o"
  "/home/eolion/ros-zz3/src/husky/husky_base/src/horizon_legacy/Message_request.cpp" "/home/eolion/ros-zz3/build/husky/husky_base/CMakeFiles/horizon_legacy.dir/src/horizon_legacy/Message_request.cpp.o"
  "/home/eolion/ros-zz3/src/husky/husky_base/src/horizon_legacy/Number.cpp" "/home/eolion/ros-zz3/build/husky/husky_base/CMakeFiles/horizon_legacy.dir/src/horizon_legacy/Number.cpp.o"
  "/home/eolion/ros-zz3/src/husky/husky_base/src/horizon_legacy/Transport.cpp" "/home/eolion/ros-zz3/build/husky/husky_base/CMakeFiles/horizon_legacy.dir/src/horizon_legacy/Transport.cpp.o"
  "/home/eolion/ros-zz3/src/husky/husky_base/src/horizon_legacy/crc.cpp" "/home/eolion/ros-zz3/build/husky/husky_base/CMakeFiles/horizon_legacy.dir/src/horizon_legacy/crc.cpp.o"
  "/home/eolion/ros-zz3/src/husky/husky_base/src/horizon_legacy/linux_serial.cpp" "/home/eolion/ros-zz3/build/husky/husky_base/CMakeFiles/horizon_legacy.dir/src/horizon_legacy/linux_serial.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"husky_base\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/eolion/ros-zz3/src/husky/husky_base/include"
  "/home/eolion/ros-zz3/devel/include"
  "/opt/ros/melodic/include"
  "/opt/ros/melodic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
