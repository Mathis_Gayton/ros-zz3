#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables
export HUSKY_MAG_CONFIG='/home/eolion/ros-zz3/src/husky/husky_bringup/config/mag_config_default.yaml'

# modified environment variables
export CMAKE_PREFIX_PATH="/home/eolion/ros-zz3/devel:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/eolion/ros-zz3/devel/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/home/eolion/ros-zz3/devel/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD='/home/eolion/ros-zz3/build'
export PYTHONPATH="/home/eolion/ros-zz3/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES='/home/eolion/ros-zz3/devel/share/common-lisp'
export ROS_PACKAGE_PATH="/home/eolion/ros-zz3/src:$ROS_PACKAGE_PATH"