#!/bin/sh

if [ -n "$DESTDIR" ] ; then
    case $DESTDIR in
        /*) # ok
            ;;
        *)
            /bin/echo "DESTDIR argument must be absolute... "
            /bin/echo "otherwise python's distutils will bork things."
            exit 1
    esac
fi

echo_and_run() { echo "+ $@" ; "$@" ; }

echo_and_run cd "/home/eolion/ros-zz3/src/pysdf"

# ensure that Python install destination exists
echo_and_run mkdir -p "$DESTDIR/home/eolion/ros-zz3/install/lib/python2.7/dist-packages"

# Note that PYTHONPATH is pulled from the environment to support installing
# into one location when some dependencies were installed in another
# location, #123.
echo_and_run /usr/bin/env \
    PYTHONPATH="/home/eolion/ros-zz3/install/lib/python2.7/dist-packages:/home/eolion/ros-zz3/build/lib/python2.7/dist-packages:$PYTHONPATH" \
    CATKIN_BINARY_DIR="/home/eolion/ros-zz3/build" \
    "/usr/bin/python2" \
    "/home/eolion/ros-zz3/src/pysdf/setup.py" \
     \
    build --build-base "/home/eolion/ros-zz3/build/pysdf" \
    install \
    --root="${DESTDIR-/}" \
    --install-layout=deb --prefix="/home/eolion/ros-zz3/install" --install-scripts="/home/eolion/ros-zz3/install/bin"
