# CMake generated Testfile for 
# Source directory: /home/eolion/ros-zz3/src
# Build directory: /home/eolion/ros-zz3/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("husky/husky_desktop")
subdirs("husky/husky_robot")
subdirs("husky/husky_simulator")
subdirs("icpr_robotsimu_results")
subdirs("husky/husky_msgs")
subdirs("pysdf")
subdirs("husky/husky_bringup")
subdirs("husky/husky_control")
subdirs("husky/husky_description")
subdirs("husky/husky_gazebo")
subdirs("husky/husky_navigation")
subdirs("husky/husky_viz")
subdirs("multikey_teleop")
subdirs("husky/husky_base")
subdirs("velocity_controllers")
